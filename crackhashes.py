
#Algorithm created to crack "/etc/shadow" password
#Author Daniel Patricio

import crypt
import sys

if(len(sys.argv) != 2):
	print 'Usage: python crackpass.py <wordlist>'
	sys.exit()
wordlist = open(sys.argv[1], 'r')
wordlist = wordlist.read().split('\n')
fullhash = raw_input("Digite o hash completo: ")
salt = raw_input("Digite o ID + salt: ")
for line in wordlist:
	tryhash = crypt.crypt(line, salt)
	if tryhash == fullhash :
		print 'Password found: '+line
		break
		sys.exit()
	else:
		print 'Tentando com: ' +tryhash